package com.example.tikhon.geyms;

public interface PrefsConst {

    String SETTINGS_GAMES_AMOUNT = "SETTINGS_GAMES_AMOUNT";
    String SETTINGS_COMPANIES_AMOUNT = "SETTINGS_COMPANIES_AMOUNT";

}
