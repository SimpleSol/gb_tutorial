package com.example.tikhon.geyms.companydetails;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tikhon.geyms.R;
import com.example.tikhon.geyms.base.BaseFragment;
import com.example.tikhon.geyms.network.GbSingleObjectResponse;
import com.example.tikhon.geyms.network.GiantBombService;
import com.example.tikhon.geyms.network.RestApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyDescriptionFragment extends BaseFragment {

    private static final String ARG_GUID = "ARG_GUID";

    private TextView tvDescription;
    private GiantBombService service = RestApi.creteService(GiantBombService.class);
    @Nullable private Call<GbSingleObjectResponse> call;

    public static CompanyDescriptionFragment newInstance(String guid) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG_GUID, guid);
        CompanyDescriptionFragment fragment = new CompanyDescriptionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("33__", "onAttach");
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_company_description;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvDescription = view.findViewById(R.id.tvDescription);
        loadCompanyDescription();
    }

    private void loadCompanyDescription() {
        if (call != null && call.isExecuted()) {
            return;
        }
        showLoading();
        @SuppressWarnings("ConstantConditions")
        String guid = getArguments().getString(ARG_GUID);
        call = service.getCompanyDetails(guid);
        //noinspection ConstantConditions
        call.enqueue(new Callback<GbSingleObjectResponse>() {
            @Override
            public void onResponse(Call<GbSingleObjectResponse> call, Response<GbSingleObjectResponse> response) {
                showContent();
                CompanyDescriptionFragment.this.call = call.clone();
                GbSingleObjectResponse gbSingleObjectResponse = response.body();
                if (gbSingleObjectResponse != null) {
                    String description = gbSingleObjectResponse.getResults().getDescription();
                    CharSequence text = description == null ? getString(R.string.no_description) : Html.fromHtml(description);
                    tvDescription.setText(text);
                }
            }

            @Override
            public void onFailure(Call<GbSingleObjectResponse> call, Throwable t) {
                showContent();
                CompanyDescriptionFragment.this.call = call.clone();
                if (!call.isCanceled()) {
                    Toast.makeText(getContext(), R.string.error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (call != null) {
            call.cancel();
        }
    }

}
