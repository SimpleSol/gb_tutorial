package com.example.tikhon.geyms.network;

import java.util.List;

public class GbObjectsListResponse {

    private List<GbObjectResponse> results;

    public List<GbObjectResponse> getResults() {
        return results;
    }
}
