package com.example.tikhon.geyms.companydetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.tikhon.geyms.R;

public class CompanyDetailsActivity extends AppCompatActivity {

    private static final String EXTRA_COMPANY_NAME = "EXTRA_COMPANY_NAME";
    private static final String EXTRA_COMPANY_GUID = "EXTRA_COMPANY_GUID";

    public static Intent makeIntent(Context context, String companyName, String guid) {
        return new Intent(context, CompanyDetailsActivity.class)
                .putExtra(EXTRA_COMPANY_NAME, companyName)
                .putExtra(EXTRA_COMPANY_GUID, guid);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_details);
        String companyName = getIntent().getStringExtra(EXTRA_COMPANY_NAME);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(companyName);
        setupViewPager();
    }

    private void setupViewPager() {
        ViewPager viewPager = findViewById(R.id.vpCompanyDetails);
        TabLayout tabLayout = findViewById(R.id.tlCompanyDetails);
        String guid = getIntent().getStringExtra(EXTRA_COMPANY_GUID);
        CompanyDetailsPagerAdapter adapter = new CompanyDetailsPagerAdapter(getSupportFragmentManager(), getResources(), guid);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
