package com.example.tikhon.geyms.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.tikhon.geyms.R;

public abstract class BaseFragment extends Fragment {

    private ViewGroup vgContent;
    private ProgressBar progressBar;

    protected abstract int getLayoutResId();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutResId(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vgContent = view.findViewById(R.id.vgContent);
        progressBar = view.findViewById(R.id.progressBar);
    }

    protected void showLoading() {
        if (vgContent != null && progressBar != null) {
            vgContent.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    protected void showContent() {
        if (vgContent != null && progressBar != null) {
            vgContent.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

}
