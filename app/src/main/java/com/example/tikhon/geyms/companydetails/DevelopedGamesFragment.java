package com.example.tikhon.geyms.companydetails;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.tikhon.geyms.R;
import com.example.tikhon.geyms.base.BaseFragment;
import com.example.tikhon.geyms.network.GbSingleObjectResponse;
import com.example.tikhon.geyms.network.GiantBombService;
import com.example.tikhon.geyms.network.RestApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DevelopedGamesFragment extends BaseFragment {

    private static final String ARG_GUID = "ARG_GUID";

    private GiantBombService service = RestApi.creteService(GiantBombService.class);
    private DevelopedGameAdapter adapter = new DevelopedGameAdapter();
    @Nullable private Call<GbSingleObjectResponse> call;

    public static DevelopedGamesFragment newInstance(String guid) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG_GUID, guid);
        DevelopedGamesFragment fragment = new DevelopedGamesFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_company_games;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = view.findViewById(R.id.rvDevelopedGames);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        loadDevelopedGames();
    }

    private void loadDevelopedGames() {
        if (call != null && call.isExecuted()) {
            return;
        }
        showLoading();
        @SuppressWarnings("ConstantConditions")
        String guid = getArguments().getString(ARG_GUID);
        call = service.getCompanyDetails(guid);
        //noinspection ConstantConditions
        call.enqueue(new Callback<GbSingleObjectResponse>() {
            @Override
            public void onResponse(Call<GbSingleObjectResponse> call, Response<GbSingleObjectResponse> response) {
                showContent();
                DevelopedGamesFragment.this.call = call.clone();
                GbSingleObjectResponse gbSingleObjectResponse = response.body();
                if (gbSingleObjectResponse != null) {
                    adapter.addAll(gbSingleObjectResponse.getResults().getDevelopedGames());
                }
            }

            @Override
            public void onFailure(Call<GbSingleObjectResponse> call, Throwable t) {
                showContent();
                DevelopedGamesFragment.this.call = call.clone();
                if (!call.isCanceled()) {
                    Toast.makeText(getContext(), R.string.error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (call != null) {
            call.cancel();
        }
    }
}
