package com.example.tikhon.geyms.companydetails;

import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.tikhon.geyms.R;

public class CompanyDetailsPagerAdapter extends FragmentPagerAdapter {

    private final String[] tabs;
    private final String guid;

    public CompanyDetailsPagerAdapter(FragmentManager fm, Resources resources, String guid) {
        super(fm);
        tabs = resources.getStringArray(R.array.company_details_tabs);
        this.guid = guid;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return CompanyDescriptionFragment.newInstance(guid);
        } else if (position == 1) {
            return DevelopedGamesFragment.newInstance(guid);
        } else {
            throw new RuntimeException("Unknown position for " + CompanyDetailsPagerAdapter.class.getSimpleName());
        }
    }

    @Override
    public int getCount() {
        return tabs.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}
