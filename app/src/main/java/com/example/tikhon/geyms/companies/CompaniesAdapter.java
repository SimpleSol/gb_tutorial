package com.example.tikhon.geyms.companies;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tikhon.geyms.R;
import com.example.tikhon.geyms.base.BaseAdapter;
import com.example.tikhon.geyms.network.GbObjectResponse;
import com.squareup.picasso.Picasso;

public class CompaniesAdapter extends BaseAdapter<GbObjectResponse, CompaniesAdapter.ViewHolder> {

    private final Callback callback;

    public CompaniesAdapter(Callback callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_company, parent, false);
        final CompaniesAdapter.ViewHolder holder = new CompaniesAdapter.ViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GbObjectResponse company = items.get(holder.getAdapterPosition());
                callback.onCompanyClick(company);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final GbObjectResponse company = items.get(position);
        Picasso.get().load(company.getImage().getSmallUrl()).into(holder.ivPicture);
        holder.tvName.setText(company.getName());
        holder.tvDeck.setText(company.getDeck());
        holder.tvCountry.setText(company.getLocationCountry());
        holder.tvCity.setText(company.getLocationCity());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivPicture;
        TextView tvName;
        TextView tvDeck;
        TextView tvCountry;
        TextView tvCity;

        public ViewHolder(View itemView) {
            super(itemView);
            ivPicture = itemView.findViewById(R.id.ivPicture);
            tvName = itemView.findViewById(R.id.tvName);
            tvDeck = itemView.findViewById(R.id.tvDeck);
            tvCountry = itemView.findViewById(R.id.tvCountry);
            tvCity = itemView.findViewById(R.id.tvCity);
        }
    }

    interface Callback {
        void onCompanyClick(GbObjectResponse company);
    }

}
