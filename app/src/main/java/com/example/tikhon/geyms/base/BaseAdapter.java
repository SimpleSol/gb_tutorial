package com.example.tikhon.geyms.base;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<ModelT, ViewHolderT extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<ViewHolderT> {

    protected List<ModelT> items = new ArrayList<>();

    public void addAll(List<ModelT> itemsToAdd) {
        items.addAll(itemsToAdd);
        notifyDataSetChanged();
    }

    public void replaceAll(List<ModelT> itemsToReplace) {
        items.clear();
        items.addAll(itemsToReplace);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
